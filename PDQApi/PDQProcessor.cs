﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace PDQApi
{
    /// <summary>
    /// Gets PDQ API and return name and drink  
    /// </summary>
    public class PDQProcessor
    {
        public static async Task<string> LoadEmployeeAsync()
        {
            ApiHelper.InitializeClient();

            string url = "https://pdqemployees.azurewebsites.net/api/pdqemployees";

            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    string comicString = await response.Content.ReadAsStringAsync();


                    return comicString;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }
}
