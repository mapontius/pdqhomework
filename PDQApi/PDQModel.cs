﻿namespace PDQApi
{
    /// <summary>
    /// Represents PDQ employee an drink 
    /// </summary>
    public class PDQModel
    {
        public string Name { get; set; }
        public string Drink { get; set; }
    }
}
