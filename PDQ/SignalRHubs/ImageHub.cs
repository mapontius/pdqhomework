﻿using Microsoft.AspNet.SignalR;

namespace PDQ.SignalRHubs
{
    /// <summary>
    /// Image Hub 
    /// </summary>
    public class ImageHub : Hub
    {
        /// <summary>
        /// Push message to clients 
        /// </summary>
        /// <param name="message">string message</param>
        public void PushChatMessageToClients(string message)
        {
            Clients.All.addChatMessage(message);
        }
    }
}