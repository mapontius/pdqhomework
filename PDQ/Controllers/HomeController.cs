﻿using GetHtmlData;
using PDQ.Models;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PDQ.Controllers
{
    /// <summary>
    /// Home controller. Shows list of employee name, drink, and has button for showing image. 
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// View of name, drink and image. 
        /// </summary>
        /// <returns>Return list view</returns>
        [HttpGet]
        public ActionResult GetEmployee()
        {
            ViewBag.Name = EmployeeModel.Name;
            ViewBag.Drink = EmployeeModel.Drink;
            HtmlProcessor rawProcessor = new HtmlProcessor();
            EmployeeModel.PicPath = @rawProcessor.RawDataHTML(EmployeeModel.Name);
            ViewBag._url = EmployeeModel.PicPath;
            return View();
        }

        /// <summary>
        /// Get new employee and update view 
        /// </summary>
        /// <param name="name">Name of employee</param>
        /// <returns>Returns employee and drink</returns>
        [HttpPost]
        public async Task<ActionResult> GetEmployee(string name)
        {
            await GetEmployeeStart.getNameAndDrinkAsync();
            ViewBag.Name = EmployeeModel.Name;
            ViewBag.Drink = EmployeeModel.Drink;
            HtmlProcessor rawProcessor = new HtmlProcessor();
            EmployeeModel.PicPath = @rawProcessor.RawDataHTML(EmployeeModel.Name);
            ViewBag._url = EmployeeModel.PicPath;
            return View();
        }

        /// <summary>
        /// Error handling page
        /// </summary>
        /// <returns>Returns error view</returns>
        public ActionResult Error()
        {
            return View();
        }
    }
}
