﻿using PDQ.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace PDQ.Controllers
{
    /// <summary>
    /// Employee APi 
    /// </summary>
    public class MyApiController : ApiController
    {
        List<EmployeeList> employeeList = new List<EmployeeList>();

        /// <summary>
        /// Set emplpoyee list 
        /// </summary>
        public MyApiController()
        {
            employeeList.Add(new EmployeeList { Name = EmployeeModel.Name, Drink = EmployeeModel.Drink, PicPath = EmployeeModel.PicPath });
        }

        /// <summary>
        /// Gets list of name, drink and image link path of employee 
        /// </summary>
        /// <returns>Returns list. Name, drink and image link path</returns>
        public List<EmployeeList> Get()
        {
            return employeeList;
        }
    }
}