﻿$(function (value) {
    // Create the connection to our SignalR-powered image Hub on the server.
    var signalRChatHub = $.connection.imageHub;

    // Add image from server.
    // Method invoked from server.
    signalRChatHub.client.addChatMessage = function (message) {

        var x = document.getElementById("image2");
        var button = document.getElementById("broadcast");

        if (x.style.display === "none") {
            x.style.display = "block";
            button.value = "Clear Image";
        }
        else if (x.style.display === "block") {
            x.style.display = "none";
            button.value = "Get Profile Image";
        }
        else {

        }
    };

    // Click event-handler for broadcasting image.
    $('#broadcast').click(function () {
        // Call Server method.
        signalRChatHub.server.pushChatMessageToClients($('#image').val());
    });

    // Start the SignalR Hub.
    $.connection.hub.start(function () {
    });
});