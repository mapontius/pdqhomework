﻿using PDQApi;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PDQ.Models
{
    /// <summary>
    /// Get employee info an set EmployeeModel values  
    /// </summary>
    public class GetEmployeeStart
    {
        public static async Task getNameAndDrinkAsync()
        {
            string x = await PDQProcessor.LoadEmployeeAsync();
            List<PDQModel> comicList = new List<PDQModel>();
            x = x.Replace("\n", "");
            x = x.Replace("\\", "");
            x = x.Replace("\"", "");
            x = x.Replace("{", "");
            x = x.Replace("}", "");

            string[] key = x.Split(',');
            string[] name = key[0].ToString().Split(':');
            string[] drink = key[1].ToString().Split(':');

            comicList.Add(new PDQModel { Name = name[1], Drink = drink[1] });

            EmployeeModel.Name = name[1];
            EmployeeModel.Drink = drink[1];
        }
    }
}