﻿namespace PDQ.Models
{
    /// <summary>
    /// Represents one person and the image 
    /// </summary>
    public class EmployeeModel
    {
        /// <summary>
        /// Name of employee 
        /// </summary>
        public static string Name { get; set; }

        /// <summary>
        /// Favorite drink 
        /// </summary>
        public static string Drink { get; set; }

        /// <summary>
        /// Path of the image 
        /// </summary>
        public static string PicPath { get; set; }
    }
}