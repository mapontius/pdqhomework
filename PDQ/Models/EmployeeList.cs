﻿namespace PDQ.Models
{
    /// <summary>
    /// Represents one person and the image 
    /// </summary>
    public class EmployeeList
    {
        /// <summary>
        /// Name of employee 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Favorite drink 
        /// </summary>
        public string Drink { get; set; }

        /// <summary>
        /// Path of the image 
        /// </summary>
        public string PicPath { get; set; }
    }
}