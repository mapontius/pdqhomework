﻿namespace GetHtmlData
{
    /// <summary>
    /// HTML image path 
    /// </summary>
    public class HtmlModel
    {
        /// <summary>
        /// Image path 
        /// </summary>
        public string ImagePath { get; set; }
    }
}
