﻿using HtmlAgilityPack;

namespace GetHtmlData
{
    /// <summary>
    /// Get HTML raw data and link to image 
    /// </summary>
    public class HtmlProcessor
    {
        public string RawDataHTML(string name)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = new HtmlDocument();
            doc = web.Load("https://www.pdq.com/about-us/");

            name = name.ToString().Replace(" ", "");
            string path = "";

            foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//img"))
            {
                HtmlAttribute attLink = link.Attributes["src"];
                HtmlAttribute attName = link.Attributes["alt"];
                string nameLink = attName.Value.ToString().Replace(" ", "");
                if (nameLink.Equals(name))
                {
                    path = attLink.Value;
                    return path;
                }
            }
            return path;
        }
    }
}
